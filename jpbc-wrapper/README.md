Fork of the JPBC library: http://gas.dia.unisa.it/projects/jpbc/

Adds support to setFromString to set an element from an input string.  Uses element_set_str.
