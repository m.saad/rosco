# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,21" size 500, 400
unset key                   # No key
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on
set format y "%.0f"
set ytics offset graph 0.02,0
set xtics offset 0,graph 0.06

set yrange [0:*]
set xrange [0:*]

set tmargin at screen 0.98
set rmargin at screen 0.98
set lmargin at screen 0.12
set bmargin at screen 0.14

# Create the plot
set xlabel "Timeout (ms)" offset 0,1.3
set ylabel "Latency (ms)" offset 3.0,0

plot  dat_file using 1:2 title 'RoSCo' with linespoints pt 7 ps 0.5 lw 3 lc 1, dat_file with errorbars ps 0 lc 1 lw 1 
#plot  dat_file using 1:($2/1000) title 'ROSCO' with linespoints lw 3 ps 1 pt 4 lc 12 
