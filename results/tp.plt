# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,25" size 500, 400
#set term svg fname "Times,21" size 600, 400
#set key at 6000,5 vertical spacing 0.65
#set key on outside reverse Left spacing 0.85 width -2
unset key
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on
set format y "%.0fK"
set yrange [0.001:*]
set xrange [0.9:288]
set logscale x 2

set ytics 1,1 offset graph 0.03,0
set xtics 1,4 offset 0,graph 0.06

set tmargin at screen 0.98
set rmargin at screen 0.97
#set rmargin at screen 0.67
set lmargin at screen 0.13
#set lmargin at screen 0.08
set bmargin at screen 0.14

# Create the plot
set xlabel "Number of Switches" offset 0,1.3
set ylabel "Throughput (Msg/s)" offset 1.9,0

set pointsize 0.4

plot dat_file using 1:($2/1000) title "Ryu" with linespoints pt 5 lw 2 lc 1, \
           '' using 1:($2/1000):($3/1000):($4/1000) notitle with errorbars pt 5 lw 1 lc 1, \
		   '' using 1:($5/1000) title "RoSCo NO-ACK" with linespoints pt 7 lw 2 lc 2, \
           '' using 1:($5/1000):($6/1000):($7/1000) notitle with errorbars pt 7 lw 1 lc 2, \
		   '' using 1:($8/1000) title "RoSCo LNZ" with linespoints pt 9 lw 2 lc 3, \
           '' using 1:($8/1000):($9/1000):($10/1000) notitle with errorbars pt 9 lw 1 lc 3, \
		   '' using 1:($11/1000) title "RoSCo WEAK-SGL" with linespoints pt 11 lw 2 lc 4, \
           '' using 1:($11/1000):($12/1000):($13/1000) notitle with errorbars pt 11 lw 1 lc 4, \
		   '' using 1:($14/1000) title "RoSCo WEAK-ALL" with linespoints pt 13 lw 2 lc 5, \
           '' using 1:($14/1000):($15/1000):($16/1000) notitle with errorbars pt 13 lw 1 lc 5, \
		   '' using 1:($17/1000) title "Ravana" with linespoints pt 15 lw 2 lc 6, \
           '' using 1:($17/1000):($18/1000):($19/1000) notitle with errorbars pt 15 lw 1 lc 6, \
		   '' using 1:($20/1000) title "RoSCo Python" with linespoints pt 17 lw 2 lc 7, \
           '' using 1:($20/1000):($21/1000):($22/1000) notitle with errorbars pt 17 lw 1 lc 7
