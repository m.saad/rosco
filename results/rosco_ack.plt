# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,21" size 500, 400
set key at 200,2.2  spacing 0.65
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on
set format y "%.0fK"
set yrange [0.0:*]
set xrange [0.9:288]
set logscale x 2

set ytics 1,1 offset graph 0.02,0
set xtics 1,4 offset 0,graph 0.05

set tmargin at screen 0.98
set rmargin at screen 0.98
set lmargin at screen 0.16
set bmargin at screen 0.14

# Create the plot
set xlabel "Number of Switches" offset 0,1.3
set ylabel "Throughput (Msg/s)" offset 2.0,0

plot dat_file using 1:5 title "NO ACK" with linespoints ps 0.5 pt 7 lw 3 lc 5, \
           '' using 1:5:6:7 notitle with errorbars ps 0.5 lw 1 lc 5, \
		   '' using 1:8 title "PAR ACK" with linespoints ps 0.5 pt 9 lw 3 lc 6, \
		   '' using 1:8:9:10 notitle with errorbars ps 0.5 pt 9 lw 1 lc 6, \
		   '' using 1:11 title "LNZ ACK" with linespoints ps 0.5 pt 11 lw 3 lc 7, \
		   '' using 1:11:12:13 notitle with errorbars ps 0.5 pt 11 lw 1 lc 7, \
		   '' using 1:14 title "SEQ ACK" with linespoints ps 0.5 pt 11 lw 3 lc 9, \
		   '' using 1:14:15:16 notitle with errorbars ps 0.5 pt 11 lw 1 lc 9
