# RoSCo
Source for the Robust SDN Controller implementation

## Setup

Implementation and test is designed for the DETERLab.  Setup requires creating and swapping in a new experiment.

To use the RoSCo Implementation a topology file is provided in 'topologies'.  Without modification, the tcl script will create a four (4) controller topology with one (1) switch node containing a smartNIC.  Additional switch and host emulation will need to be performed using mininet.

## RoSCo Java Implementation Setup
It is recommended that after setting up the experiment, to use the java implementation of RoSCo.  For setup instructions follow the README.md in the java directory.

The RoSCo java implementation is not dependent on any specific controller however the scripts are set up to force the java RoSCo layer to connect to a controller on localhost with port 4000.  The test directory is also set up to us Ryu.  When installing Ryu via the Ubuntu package manager (apt-get) 

sudo apt-get install ryu-bin

you will need to modify the ryu configuration file prior to running scripts in the test directory.  The modification that needs to be made is in /etc/ryu/ryu.conf.  Edit the file as root and comment out or remove the include 

sudo vim /etc/ryu/ryu.conf

log_config_file=/etc/ryu/log.conf

The BLS implementation requires PBC from https://crypto.stanford.edu/pbc/.  This software has a dependency of libgmp

sudo apt-get install libgmp-dev

## Switch Setup

* The Ubuntu-Agilio-2 OS image used by the 'rosco_smartnic_sdn.tcl' includes an implementation of Open vSwitch with smartNIC extensions.  To use RoSCo this will need to be overwritten with the version of Open vSwitch with smartNIC and RoSCo extensions.  The source of which is located in the robust-sdn/ovs-agilio-rosco repository

* To emulate switches and hosts, mininet is required.  Unfortunately, the mininet package requires the Open vSwitch package to be installed which is NOT since the version of Open vSwitch used is that with smartNIC and RoSCo extensions, not the one provided by the Ubuntu repositories.  It is recommended that you download a dpkg for the most recent version of mininet or build one yourself (http://mininet.org/download/).
	1. Ensure that Open vSwitch from the robust-sdn/ovs-agilio-rosco is built and installed
	1. Install the mininet dependencies
	
	   sudo apt-get install socat iperf cgroup-bin
	   
	1. Run: sudo dpkg -i --ignore-depends=openvswitch-switch mininet/mininet_....._amd64.deb

* Running the dpkg command with the ignore-depends will allow mininet to be installed without requiring the openvswitch-switch package
