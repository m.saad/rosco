#!/usr/bin/perl

use strict;

use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

my $controller_nodes = get_lines_from_file("$proj_dir/java/config/controllernodelist");

my $script = shift;
$script = "$script_dir/$script";

for(@{$controller_nodes}) {
	print "Running $script on $_\n";
	system("ssh -oStrictHostKeyChecking=no $_ $script");
}
