#!/usr/bin/perl

use strict;
use Data::Dumper;
use File::Basename;
use Cwd 'abs_path';

my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

my @unused_subnets = ('127.0', '192.168');

my $bridge_name = "br0";
my $hosts_file = "/etc/hosts";
my $switch_nodes = "$proj_dir/java/config/switchnodelist";
my $controller_nodes = "$proj_dir/java/config/controllernodelist";

my $switchlist =  get_lines_from_file($switch_nodes);
my $controllerlist =  get_lines_from_file($controller_nodes);

my $ifaces = getIFaceIPs();
my $controller_ips = getHostIPs($controllerlist);

my $filtered_ifaces = filterInterfaces($ifaces, $controller_ips);

#print Data::Dumper->Dump([$filtered_ifaces]);

die "Could not find any interfaces for bridge.\n" unless (defined($filtered_ifaces));

system_or_continue("sudo ovs-vsctl br-exists $bridge_name");
my $bridge_exists = $? >> 8;
if($bridge_exists != 2) {
	system_or_die("sudo ovs-vsctl del-br $bridge_name");
}

for(@{$filtered_ifaces}) {
	system_or_die("sudo ip addr flush dev $_");
}

system_or_die("sudo ovs-vsctl add-br $bridge_name");
addInterfacesToBridge($filtered_ifaces, $bridge_name);
addControllersToBridge($controller_ips, $bridge_name);

{
	my @added_interfaces = ();
	sub addInterfacesToBridge {
		my ($ifaces, $bridge) = @_;
		for(@{$ifaces}) {
			my $iface = $_;
			unless(grep(/$iface/, @added_interfaces)) {
				system_or_die("sudo ovs-vsctl add-port $bridge $iface");
				push @added_interfaces, $iface;
			}
		}
	}
}
{
	my @added_controllers = ();
	sub addControllersToBridge {
		my ($hosts, $bridge) = @_;
		for(values %{$hosts}) {
			my $controller = $_;
			unless(grep(/$controller/, @added_controllers)) {
				push @added_controllers, $controller;
			}
		}	
		system_or_die("sudo ovs-vsctl set-controller $bridge " . join(" ", map { "tcp:$_:6633" } @added_controllers));
	}
}

sub _is_on_subnet {
	my $ip = shift;
	my $ips = shift;

	return 0 unless($ip);

	my $ip_subnet = $ip;
	$ip_subnet =~ s/\.[0-9]+$//;

	for(values %{$ips}) {
		my $search_subnet = $_;
		$search_subnet =~ s/\.[0-9]+$//;
		if($ip_subnet eq $search_subnet) {
			return 1;
		}
	}
	return 0;
}

sub _is_on_unused_subnet {
	my $ip = shift;

	return 0 unless ($ip);

	for(@unused_subnets) {
		if(begins_with($ip, $_)) {
			return 1;
		}
	}

	return 0;
}

sub filterInterfaces {
	my $ifaces = shift;
	my $ips = shift;

	my $filtered_ifaces;

	for(keys %{$ifaces}) {
		my $iface = $_;
		next if ($iface eq $bridge_name);
		my $ip = $ifaces->{$iface};
		next if _is_on_subnet($ip, $ips);
		next if _is_on_unused_subnet($ip);
		push @{$filtered_ifaces}, $iface;
	}

	return $filtered_ifaces;
}
	

sub begins_with {
	return substr($_[0], 0, length($_[1])) eq $_[1];
}

sub iterator_to_ip {
        my $value = shift;
		my $base_ip = shift;
        my @ip = ();

        my $mod_max = 256 - $base_ip;

        for(0 .. 3) {
                $ip[$_] = (($value / ($mod_max**$_)) % $mod_max) + $base_ip;
        }
        return join(".", reverse @ip);
}
