#!/usr/bin/perl

use strict;
use Data::Dumper;
use File::Basename;
use Cwd 'abs_path';

my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

my $host_nodes = "$proj_dir/java/config/hostnodelist";

my $local_iface = _find_local_host_iface();
die "Unable to find local host network interface on VLAN.\n" unless(defined($local_iface));

my $hostlist =  get_lines_from_file($host_nodes);
my $host_ips = getHostIPs($hostlist);
my $filtered_ips = filterIPs($host_ips);

for(@{$filtered_ips}) {
	my $ip_subnet = $_;
	$ip_subnet =~ s/\.[0-9]+$//;
	system_or_die("sudo route add -net $ip_subnet.0 netmask 255.255.255.0 dev $local_iface");
}

sub filterIPs {
	my $ips = shift;
	my $filtered_ips;
	
	my $ifaces = getIFaceIPs();
	for(values %{$ips}) {
		next if(_is_on_local_host($ifaces, $_));
		push @{$filtered_ips}, $_;
	}

	return $filtered_ips;
}
	
sub _find_local_host_iface {
	my $hostname = `/bin/hostname -s`;
	chomp $hostname;

	my $local_ip = getHostIPs([$hostname])->{$hostname};

	return unless defined($local_ip);

	my $ifaces = getIFaceIPs();
	for(keys %{$ifaces}) {
		if($ifaces->{$_} eq $local_ip) {
			return $_;
		}
	}

	return undef;
}

sub _is_on_local_host {
	my $ifaces = shift;
	my $ip = shift;
	for(values %{$ifaces}) {
		if($ip eq $_) {
			return 1;
		}
	}
	return 0;
}

