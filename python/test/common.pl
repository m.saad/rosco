use strict;

my $project_path = "/proj/Rosco";
my $smart_bft_path = "$project_path/rosco/smartBFT";
my $rosco_app_path = "$project_path/rosco/sdn-scripts/RoscoApp";
my $rosco_test_path = "$rosco_app_path/test";
my $host_config = "$smart_bft_path/config/hosts.config";
my $log_dir = "/tmp/rosco/log";

# Controller utilities
sub getControllers {
	my $controllers;
	my @controller_ips = (
		'10.1.1.3',
		'10.1.1.4',
		'10.1.1.5',
		'10.1.1.6'
		#'10.1.2.3',
		#'10.1.2.4',
		#'10.1.2.5',
		#'10.1.2.6'
	);
	my $count = 0;
	for(@controller_ips) {
		$controllers->{$count++} = $_;
	}	
	return $controllers;
}

sub startBftServers {
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
		my $id = $_;
		my $ctl = $controllers->{$id};
		print "Starting RoSCo BFT $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_test_path/startroscobft $id $log_dir");
	}
}
sub stopBftServers {
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
        my $id = $_;
        my $ctl = $controllers->{$id};
        print "Stopping RoSCo BFT $id $ctl\n";
        system_or_exit("ssh $ctl $rosco_test_path/stoproscobft $id");
	}
}

sub startRoscoServers {
	my $ctl_app = shift;
	my $log_level = shift;
	my $dpset_size = shift;
	my $batch_size = shift;
	my $batch_timeout = shift;
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
		my $id = $_;
		my $ctl = $controllers->{$id};
		print "Starting RoSCo Ryu $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_test_path/startroscoctl $id $rosco_app_path/$ctl_app.py $log_dir $log_level $dpset_size $batch_size $batch_timeout");
	}
}
sub stopRoscoServers {
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
		my $id = $_;
		my $ctl = $controllers->{$id};
		print "Stopping RoSCo Ryu $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_test_path/stoproscoctl");
	}
}
sub startRoscoServersWithMalicious {
	my $ctl_app = shift;
	my $log_level = shift;
	my $dpset_size = shift;
	my $batch_size = shift;
	my $batch_timeout = shift;
	my $controllers = getControllers();
	my $last_ctl = scalar(keys %{$controllers}) - 1;
	for (sort keys %{$controllers}) {
		my $id = $_;
		last if $id eq $last_ctl;
		my $ctl = $controllers->{$id};
		print "Starting RoSCo Ryu $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_test_path/startroscoctl $id $rosco_app_path/$ctl_app.py $log_dir $log_level $dpset_size $batch_size $batch_timeout");
	}
	print "Starting RoSCo Malicious\n";
	my $ctl = $controllers->{$last_ctl};
	system_or_exit("ssh $ctl $rosco_test_path/startroscomal");
}
sub stopRoscoServersWithMalicious {
	stopRoscoServers;
	my $controllers = getControllers();
	my $last_ctl = scalar(keys %{$controllers}) - 1;
	print "Stopping RoSCo Malicious\n";
	my $ctl = $controllers->{$last_ctl};
	system_or_exit("ssh $ctl $rosco_test_path/stoproscomal");
}

sub startControllers {
	my $controllerApp = shift;
	my $loglevel = shift;
	my $dpset_size = shift;
	my $batch_size = shift;
	my $batch_timeout = shift;
	startBftServers();
	sleep 15;
	startRoscoServers($controllerApp, $loglevel, $dpset_size, $batch_size, $batch_timeout);
}
sub startControllersWithMalicious {
	my $controllerApp = shift;
	my $loglevel = shift;
	my $dpset_size = shift;
	my $batch_size = shift;
	my $batch_timeout = shift;
	startBftServers();
	sleep 15;
	startRoscoServersWithMalicious($controllerApp, $loglevel, $dpset_size, $batch_size, $batch_timeout);
}
sub stopControllers {
	stopRoscoServers();
	stopBftServers();
}
sub stopControllersWithMalicious {
	stopRoscoServersWithMalicious;
	stopBftServers;
}
sub restartControllers {
	stopControllers;
	startControllers(@_);
}
sub restartControllersWithMalicious {
	stopControllersWithMalicious;
	startControllersWithMalicious(@_);
}

# Mininet utilities
sub reset_mn {
	system_or_exit("sudo mn -c");
	if($? != 0) {
		die "Unable to clean mininet config: $!\n";
	}
}
sub reset_exec_mn {
	my $script = shift;
	my ($mn_out, $mn_in);

	reset_mn;
	print "Starting mininet: $script\n";
	my $python_cmd = "sudo python -u $rosco_test_path/$script @_";
	print "$python_cmd\n";
	my $mn_pid = open3($mn_in, $mn_out, $mn_out, $python_cmd);

	my $inchar;
	my $loc = 0;
	while(1) {
		read $mn_out, $inchar, 1, $loc;
		last if($inchar =~ /mininet> /);
		if($inchar =~ /\n$/) {
			print $inchar;
			$inchar = "";
			$loc = 0;
		} else {
			$loc++;
		}
	}

	return ($mn_pid, $mn_out, $mn_in);
}
sub exit_mn {
	my ($mn_pid, $mn_out, $mn_in, $ovs_out) = @_;
	print $mn_in "exit\n";
	if(defined($ovs_out)) {
		wait_for_end_ovs_log($ovs_out, 1);
		kill 'INT', $mn_pid;
	}
	waitpid $mn_pid, 0;
}

sub system_or_exit {
	my $cmd = shift;
	if(do_system($cmd) != 0) {
		die "$cmd failed: $?\n";
	}
}

sub do_system {
	my $cmd = shift;
	print("$cmd\n");
	my $retval = system($cmd);
	return $retval >> 8;
	#return 0;
}

return 1;
