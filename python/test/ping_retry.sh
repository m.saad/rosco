#/bin/bash

retries=1
until ping -c 1 $1 ; do echo RT: $retries ; retries=$((retries+1)); false ; done
