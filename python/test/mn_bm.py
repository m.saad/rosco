import sys
import os
import time

from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, RemoteController
from mininet.cli import CLI
from mininet.log import setLogLevel, info

PORT = 6633

def multiCtrlNet(controllers, switch_count):

    net = Mininet(controller=RemoteController, switch=OVSKernelSwitch)

    c = [ net.addController('c%s' % ci, controller=RemoteController,ip=controllers[ci-1], port=PORT)
            for ci in range(1, len(controllers) + 1) ]

    s = [ net.addSwitch( 's%s' % si )
            for si in range(1, switch_count + 1) ]

    last = None
    for switch in s:
        if last:
            net.addLink(last, switch)
        last = switch

    net.build()

    for ctrl in c:
        ctrl.start()

    for switch in s:
        switch.start(c);
        time.sleep(1)

    net.start()
    net.staticArp()
    CLI( net )
    net.stop()

if __name__ == '__main__':
    
    if(len(sys.argv) < 2):
         raise SystemExit('Usage: %s switch_count controller_ips' % os.path.basename(__file__))

    #setLogLevel( 'debug' )
    multiCtrlNet(sys.argv[2:], int(sys.argv[1]))

