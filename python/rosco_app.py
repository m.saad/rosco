# Copyright (C) 2017 James Lembke
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_0
from ryu.ofproto import ofproto_v1_2
from ryu.ofproto import ofproto_v1_3
from ryu.ofproto import ofproto_v1_4
from ryu.ofproto.ofproto_parser import MsgBase
from ryu.ofproto.ofproto_v1_3_parser import *
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet

import six
import os
import time
import uuid
import pprint
import sys
import threading
import signal
import subprocess
from ryu.controller import dpset
from ryu.lib import hub
import logging
from rosco_event_log import *
#from rosco_event_log_nobatch import *

# Force max XID for all protocol versions
ROSCO_MAX_XID = 0x7fffffff
ROSCO_XID_MASK = 0x80000000
ROSCO_BENCHMARK = 25
ofproto_v1_0.MAX_XID = ROSCO_MAX_XID
ofproto_v1_2.MAX_XID = ROSCO_MAX_XID
ofproto_v1_3.MAX_XID = ROSCO_MAX_XID
ofproto_v1_4.MAX_XID = ROSCO_MAX_XID

# This is merely for API compatibility on python2
if six.PY3:
        buffer = bytes

class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class RoscoApp(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    _CONTEXTS = {
            'dpset': dpset.DPSet,
            }

    def __init__(self, *args, **kwargs):
        super(RoscoApp, self).__init__(*args, **kwargs)

        try:
            client_port = int(os.environ['CLIENT_PORT'])
        except KeyError:
            client_port = 5000

        try:
            max_batch_size = int(os.environ['BATCH_SIZE'])
        except KeyError:
            max_batch_size = 1000

        try:
            batch_timeout = float(os.environ['BATCH_TIMEOUT'])
        except KeyError:
            batch_timeout = 0.1

        self.dpset_size = 0
        try: 
            self.dpset_size = int(os.environ['DPSET_SIZE'])
        except KeyError:
            self.dpset_size = 0

        self.logger.info("DPSET SIZE: %d" % self.dpset_size)
        #self.logger.info("CLIENT PORT: %d" % client_port)

        self.dpset = kwargs['dpset']
        self.msg_handlers = {}
        self.unacked_msgs = dict()
        self._xids = dict()
        self.ack_conds = dict()
        self.send_mutex = threading.Lock()

        self.controller_id = uuid.uuid4()
        self.logger.info('Initializing controller: %s Batch Size: %d Batch Timeout: %f' % (self.controller_id.hex, max_batch_size, batch_timeout))
    
        self.event_log = RoscoEventLog(client_port, max_batch_size, batch_timeout, self)
        self.threads.append(hub.spawn(self.event_log.send_data))
        self.threads.append(hub.spawn(self.event_log.receive_data))
   
    def log_benchmark(self, value):
        self.logger.log(ROSCO_BENCHMARK, value)

    def event_loop(self, dpid):
      
        if(self.dpset_size > 0):
            while(len(self.unacked_msgs) != self.dpset_size):
                time.sleep(1)

        self.logger.warning("%d ready" % dpid)

        while True:
    
            # Ack barrier for independent switch updates
            #self.ack_conds[dpid].acquire()
            #while len(self.unacked_msgs[dpid]) > 0:
            #    #self.logger.warning(self.unacked_msgs[dpid].keys())
            #    self.ack_conds[dpid].wait()
            #self.ack_conds[dpid].release()

            # Ack barrier for all updates dependent on all others
            #for alldp in self.unacked_msgs:
            #    self.ack_conds[alldp].acquire()
            #    while(len(self.unacked_msgs[alldp]) > 0 and min(self.unacked_msgs[alldp].keys()) < self._xids[dpid]):
            #        #print self.unacked_msgs[dpid].keys()
            #        #print "%d waiting %d" % (dpid, alldp)
            #        #print self.unacked_msgs[alldp].keys()
            #        self.ack_conds[alldp].wait()
            #        #print "%d notified %d" % (dpid, alldp)
            #    self.ack_conds[alldp].release()
            
            #self.logger.warning("%d PAST BARRIER: WAITING FOR EVENT" % dpid)

            msg = self._deserialize_msg(self.event_log.get_next_event(dpid))
            ev_type = "Event" + msg.__class__.__name__

            if(ev_type == "EventNoneType"):
                continue

            #self.logger.info("event_loop " + msg_str)
            #self.logger.info(ev_type)

            try:
                #self.logger.warning("TIME CLIENT IN: %d : %d : %f" % (dpid, msg.xid, time.time()))
                ev_handler = self.msg_handlers[ev_type]
                cmds = ev_handler(msg)
                #self.logger.warning("TIME CLIENT OUT: %d : %d : %f" % (dpid, msg.xid, time.time()))
                self.do_send_cmds(cmds, msg)

            except KeyError:
                self.logger.error("Handler not found for message: %s." % ev_type)
                #self.logger.error(msg_str)

    def do_send_cmds(self, cmds, inmsg):
        if cmds is None:
            return
        self.send_mutex.acquire()
        for cmd in cmds:
            self.do_send_cmd(cmd, inmsg)
        self.send_mutex.release()

    def do_send_cmd(self, msg, inmsg):
        datapath = msg.datapath
        msg.xid = self._xids[datapath.id] | ROSCO_XID_MASK
        self.unacked_msgs[datapath.id][self._xids[datapath.id]] = msg;
        
        #self.logger.warning("TIME SEND: %d : %d : %d : %f" % (datapath.id, self._xids[datapath.id], inmsg.xid, time.time()))

        #self.logger.warning("Sending message (datapath, xid, unacked, time) = (%d, %d, %d, %f)" % (datapath.id, self._xids[datapath.id], len(self.unacked_msgs[datapath.id]), time.time()))
        #print self.unacked_msgs[datapath.id].keys()
        #print "DP %d XID %d" % (datapath.id, self._xids[datapath.id])
        
        self._xids[datapath.id] += 1
        self._xids[datapath.id] &= ROSCO_MAX_XID

        datapath.send_msg(msg)

    @set_ev_cls(dpset.EventDP)
    def _dp_handler(self, ev):
        if(ev.enter):
            datapath = ev.dp
            ofproto = datapath.ofproto
            ofproto.MAX_XID = 0x7fffffff
            if(datapath.id not in self._xids):
                self._xids[datapath.id] = 1
                self.unacked_msgs[datapath.id] = dict()
                self.ack_conds[datapath.id] = threading.Condition()
                self.event_log.add_queue(datapath.id)
                self.threads.append(hub.spawn(self.event_loop, datapath.id))

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def _switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.
        if ofproto.OFP_VERSION >= ofproto_v1_2.OFP_VERSION:
            match = parser.OFPMatch()
            actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                ofproto.OFPCML_NO_BUFFER)]
            inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                actions)]
            mod = parser.OFPFlowMod(datapath=datapath, priority=0,
                    match=match, instructions=inst)	
            datapath.send_msg(mod)


    def handle_event(self, ev):
        #self.logger.warning("DATAPATH, XID %d %d" % (ev.msg.datapath.id, ev.msg.xid))
        self.event_log.put(ev.msg.datapath.id, self._serialize_msg(ev.msg), ev.msg.xid)

    @set_ev_cls(ofp_event.EventOFPRoleReply, MAIN_DISPATCHER)
    def _role_reply_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPErrorMsg, MAIN_DISPATCHER)
    def _error_handler(self, ev):
        self.handle_event(ev)

# No concensus needed for echo messages
#  They are handled automatically by Ryu
#    @set_ev_cls(ofp_event.EventOFPEchoRequest, MAIN_DISPATCHER)
#    def _echo_request_handler(self, ev):
#	self.handle_event(ev)
#    @set_ev_cls(ofp_event.EventOFPEchoReply, MAIN_DISPATCHER)
#    def _echo_reply_handler(self, ev):
#	self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPGetConfigReply, MAIN_DISPATCHER)
    def _get_config_reply_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        #self.logger.warning("%d %d PACKET IN: XID, DP" % (ev.msg.xid, ev.msg.datapath.id))
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPFlowRemoved, MAIN_DISPATCHER)
    def _flow_removed_handler(self, ev):
        self.handle_event(ev)

#    @set_ev_cls(ofp_event.EventOFPPortStatus, MAIN_DISPATCHER)
#    def _port_status_handler(self, ev):
#        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPDescStatsReply, MAIN_DISPATCHER)
    def _desc_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def _flow_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPAggregateStatsReply, MAIN_DISPATCHER)
    def _aggregate_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPTableStatsReply, MAIN_DISPATCHER)
    def _table_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPPortStatsReply, MAIN_DISPATCHER)
    def _port_stats_handler(self, ev):
        self.handle_event(ev)

    #@set_ev_cls(ofp_event.EventOFPQueueStatsReply, MAIN_DISPATCHER)
    #def _queue_stats_handler(self, ev):
    #    self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPQueueStatsReply, MAIN_DISPATCHER)
    def _queue_stats_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        xid = int(msg.xid) & ~ROSCO_XID_MASK
        
        #self.logger.warning("Receive cmd ack (datapath, xid, unacked, time) = (%d, %d, %d, %f)" % (datapath.id, xid, len(self.unacked_msgs[datapath.id]), time.time()))
        #print self.unacked_msgs[datapath.id].keys()
        #self.logger.warning("TIME ACK: %d : %d : %f" % (datapath.id, xid, time.time()))

        self.ack_conds[datapath.id].acquire()
        self.unacked_msgs[datapath.id].pop(xid, None)
        self.ack_conds[datapath.id].notifyAll()
        self.ack_conds[datapath.id].release()

    @set_ev_cls(ofp_event.EventOFPGroupStatsReply, MAIN_DISPATCHER)
    def _group_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPGroupDescStatsReply, MAIN_DISPATCHER)
    def _group_desc_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPGroupFeaturesStatsReply, MAIN_DISPATCHER)
    def _group_features_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPMeterStatsReply, MAIN_DISPATCHER)
    def _meter_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPMeterConfigStatsReply, MAIN_DISPATCHER)
    def _meter_config_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPMeterFeaturesStatsReply, MAIN_DISPATCHER)
    def _meter_features_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPBarrierReply, MAIN_DISPATCHER)
    def _barrier_reply_handler(self, ev):
        ev_type = "EventOFPBarrierReply"
        try:
            ev_handler = self.msg_handlers[ev_type]
            ev_handler(ev.msg)
        except KeyError:
            self.logger.error("Handler not found for message: %s." % ev_type)
        #self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPPortDescStatsReply, MAIN_DISPATCHER)
    def _port_desc_stats_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPQueueGetConfigReply, MAIN_DISPATCHER)
    def _queue_get_config_reply_handler(self, ev):
        self.handle_event(ev)

    @set_ev_cls(ofp_event.EventOFPGetAsyncReply, MAIN_DISPATCHER)
    def _get_async_reply_handler(self, ev):
        self.handle_event(ev)

    def _serialize_msg(self, msg):
        assert isinstance(msg, MsgBase)
        res = {"cls": msg.__class__.__name__,
                "datapath": msg.datapath.id,
                "version": msg.version,
                "msg_type": msg.msg_type,
                "msg_len": msg.msg_len,
                "xid": msg.xid,
                #PYTHON 2
                #"buf": str(msg.buf)}
                #PYTHON 3
                "buf": msg.buf}
        return str(res)

    def _deserialize_msg(self, value):
        import ast
        msg_dict = ast.literal_eval(value)

        dpid = msg_dict["datapath"]
        for (_dpid, dp) in self.dpset.get_all():
            if(_dpid == dpid):
                datapath = dp

        msg_class = eval(msg_dict["cls"])

        try:
            msg = msg_class.parser(datapath,
                msg_dict["version"], msg_dict["msg_type"],
                #PYTHON 2
                #msg_dict["msg_len"], msg_dict["xid"], buffer(msg_dict["buf"]))
                #PYTHON 3
                msg_dict["msg_len"], msg_dict["xid"], msg_dict["buf"])
        except UnboundLocalError:
            return None

        return msg
