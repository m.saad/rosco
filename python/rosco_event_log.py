import socket
#import Queue as Queue
import queue as Queue
import collections
import time
import threading
import json
import _thread as thread
#import thread as thread
import ast
from ryu.lib import hub

class RoscoEventLog(object):
    """Distributed shared event log"""
    
    def __init__ (self, port, mbs, to, app=None):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(("localhost", port))
        self.msg_queues = dict()
        self.watermarks = dict()
        self.ev_mutex = threading.Lock()
        self.send_queue = Queue.Queue()
        self.send_mutex = threading.Condition()
        self.send_order = 0
        self.last_sent_batch = -1
        self.max_batch = mbs
        self.batch_timeout = to
        self.batch_size = 0
        self.rosco_app = app

    def _socket_send(self, send_data):
        totalsent = 0
        while totalsent < len(send_data):
            sent = self.sock.send(send_data[totalsent:].encode())
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + sent

    def _socket_recv_data(self, size):
        recv_data = ""
        size_remaining = size
        while(size_remaining > 0):
            chunk = self.sock.recv(size_remaining).decode('utf-8')
            if chunk == '':
                raise RuntimeError("socket connection broken")
            recv_data += chunk
            size_remaining -= len(chunk)
        return recv_data

    def _socket_recv_line(self):
        recv_data = ""
        while "\n" not in recv_data:
            chunk = self.sock.recv(1).decode('utf-8')
            if chunk == '':
                raise RuntimeError("socket connection broken")
            recv_data += chunk
        return recv_data.rstrip('\n');

    def mergeMsgsToBuffer(self, msgs):
        #return "|<>|".join(msgs)
        return json.dumps(msgs)

    def splitBufferToMsgs(self, buf):
        #return buf.split("|<>|")
        return json.loads(buf)

    def _send_data_buffer(self, send_str, batch_id):
        self.send_mutex.acquire()
        while(self.last_sent_batch+1 != batch_id):
            self.send_mutex.wait()
        #print "SEND: %d %f" % (batch_id, time.time())
        self._socket_send("%d\n%s\n" % (len(send_str)+1, send_str))
        #self._socket_send("%d\n" % (len(send_str)+1))
        #self._socket_send("%s\n" % (send_str))
        self.last_sent_batch = batch_id
        #print "SEND DONE: %d %f" % (batch_id, time.time())
        self.send_mutex.release()

    def add_queue(self, dpid):
        self.ev_mutex.acquire()
        if(dpid not in self.msg_queues):
            self.msg_queues[dpid] = Queue.Queue()
            self.watermarks[dpid] = -1
        self.ev_mutex.release()

    def put(self, dpid, msgdata, mid):
        if(dpid not in self.watermarks):
            self.add_queue(dpid)
        
        if(mid <= self.watermarks[dpid]):
            return

        #print "TIME IN: %d : %d : %f" % (dpid, mid, time.time())

        self.send_queue.put((dpid, mid, msgdata))

    def receive_data(self):
        while True:
            msg_len = int(self._socket_recv_line())
            msgdata = self._socket_recv_data(msg_len)
            msgdata = msgdata.rstrip('\n')

            msg_strs = self.splitBufferToMsgs(msgdata)
            for msg in msg_strs:
                msg_dict = ast.literal_eval(msg)

                dpid = msg_dict["datapath"]
        
                if(dpid not in self.msg_queues):
                    self.add_queue(dpid)
               
                #if(self.rosco_app is not None):
                #    self.rosco_app.logger.warning("RECEIVE %d %d" % (msg_dict["xid"], self.watermarks[dpid]))
                
                if(msg_dict["xid"] <= self.watermarks[dpid]):
                    continue
               
                self.watermarks[dpid] = msg_dict["xid"]
                self.msg_queues[msg_dict["datapath"]].put(msg)
                
                #self.logger.warning("TIME OUT: %d : %d : %f : %s" % (msg_dict["datapath"], msg_dict["xid"], time.time(), msg_dict["cls"]))

    def send_data(self):
        while True:
            batch_size = 0
            batch_buffer = []

            (dpid, mid, msgdata) = self.send_queue.get(block=True)
            if(mid <= self.watermarks[dpid]):
                continue

            #print "BATCH START: %f" % time.time()

            batch_buffer.append(msgdata)
            batch_size += 1

            if(batch_size < self.max_batch):
                start_time = time.time()
                remaining_time = self.batch_timeout
                #self.rosco_app.log_benchmark("START BATCH: %f %f" % (start_time, remaining_time))
                while remaining_time > 0:
                    try:
                        #self.rosco_app.log_benchmark("SLEEPING REMAINING TIME: %f" % remaining_time)
                        (dpid, mid, msgdata) = self.send_queue.get(block=True, timeout=remaining_time)
                        remaining_time = start_time - time.time() + self.batch_timeout
                        #if(remaining_time < 0.001):
                        #    self.rosco_app.log_benchmark("IMPL TIMEOUT: %f" % (time.time() - start_time))
                        
                        if(mid <= self.watermarks[dpid]):
                            continue

                        batch_buffer.append(msgdata)
                        batch_size += 1

                        if(batch_size >= self.max_batch):
                            break

                    except Queue.Empty:
                        #self.rosco_app.log_benchmark("TIMEOUT: %f" % (start_time - time.time() + self.batch_timeout))
                        break
                #if (self.rosco_app is not None):
                #    self.rosco_app.log_benchmark("BATCH TIME: %f %f %d %d" % ((time.time() - start_time), self.batch_timeout, batch_size, self.max_batch))
                #print ("BATCH TIME:", (time.time() - start_time), batch_size, self.max_batch)
                self.rosco_app.logger.warning("BATCH TIME: %f %d" % ((time.time() - start_time), batch_size))

            self.send_mutex.acquire()
            #self.rosco_app.logger.warning("BATCH TIME: %f %d %d" % ((time.time() - start_time), batch_size, self.max_batch))
            if(self.rosco_app is not None):
                self.rosco_app.threads.append(hub.spawn(self._send_data_buffer, self.mergeMsgsToBuffer(batch_buffer), self.send_order))
            else:
                thread.start_new_thread(self._send_data_buffer, (self.mergeMsgsToBuffer(batch_buffer), self.send_order))
            self.send_mutex.notifyAll()
            self.send_mutex.release()
            self.send_order += 1

    def get_next_event(self, dpid):
        msg = self.msg_queues[dpid].get(block=True)
        return msg

if __name__ == '__main__':
    import sys
    import random
    import string

    batch_size = int(sys.argv[1])
    batch_timeout = float(sys.argv[2])
    total_batches = int(sys.argv[3])

    total_messages = batch_size * total_batches

    rel = RoscoEventLog(5000, batch_size, batch_timeout)
    thread.start_new_thread(rel.send_data, ())
    thread.start_new_thread(rel.receive_data, ())
        
    msg_contents = []
    for y in range(0, total_batches):
        msg_contents.append([])
        for x in range(0, batch_size):
            msg_contents[y].append(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(300)))

    mxid = 1
    start_time = time.time()
    for y in range(0, total_batches):

        #print "FIB: %d" % mxid
        time_start = time.time()
        #print "TIME IN: %d : %d : %f" % (0, mxid, time.time())
        fib = mxid
        for x in range(0, batch_size):
            res = {"cls": "OFPPacketIn",
                    "datapath": 0,
                    "version": 0,
                    "msg_type": 0,
                    "msg_len": 0,
                    "xid": mxid,
                    "buf": msg_contents[y][x]}
            rel.put(0, str(res), mxid)
            mxid += 1
        for x in range(0, batch_size):
            msg = rel.get_next_event(0)
            if(x == 0):
                print("SENT LATENCY:", time.time() - time_start, batch_size)
                #print "SENT LATENCY: %f %d" % (time.time() - time_start, batch_size)
                #print "TIME OUT: %d : %d : %f" % (0, fib, time.time())

    throughput_time = time.time() - start_time
    print("THROUGHPUT:", throughput_time, batch_size)
    #print"THROUGHPUT: %f %d" % (throughput_time, batch_size)

