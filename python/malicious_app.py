import socket
import threading
import _thread as thread
#import thread as thread
from rosco_event_log_nobatch import *
from time import sleep

import random
import string

rel = RoscoEventLog(5000, 0, 0)
thread.start_new_thread(rel.send_data, ())

msg_contents = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(300))
res = {"cls": "OFPPacketIn",
        "datapath": 0,
        "version": 0,
        "msg_type": 0,
        "msg_len": 0,
        "xid": 0,
        "buf": msg_contents}

while(True):
    rel.put(0, str(res), 0)
    sleep(0.001)
