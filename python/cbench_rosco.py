from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_0
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet

import rosco_app

class CBenchRosco(rosco_app.RoscoApp):
    OFP_VERSIONS = [ofproto_v1_0.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(CBenchRosco, self).__init__(*args, **kwargs)

        self.mod = None
        self.msg_handlers = {"EventOFPPacketIn": self.packet_in_handler}

    def packet_in_handler(self, msg):
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        #print "Received %d" % datapath.id

        #match = datapath.ofproto_parser.OFPMatch(
                #    ofproto_v1_0.OFPFW_ALL, 0, 0, 0,
                #    0, 0, 0, 0, 0, 0, 0, 0, 0)

        match = datapath.ofproto_parser.OFPMatch()

        mod = datapath.ofproto_parser.OFPFlowMod(
            datapath, match=match, cookie=0, command=ofproto.OFPFC_ADD,
            idle_timeout=0, hard_timeout=0,
            priority=ofproto.OFP_DEFAULT_PRIORITY,
            flags=0)

        return (mod, )
