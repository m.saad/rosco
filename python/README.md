# rosco-python
Python implementation of the RoSCo Robust SDN Controller

## Setup

Set up RSA key pairs for controllers so that ssh works without a password

Install java components
 * sudo apt-get install ant
 * sudo apt-get install default-jdk

Install Ryu 4.23 for python3

Edit config/hosts.config to set the IP addresses of all replicas

## Test Scripts (in /test)
NOTE: ensure that /tmp/rosco/ does not exist on each controller

* startallctl.pl - start all RoSCo Controllers

  Usage: startallctl.pl batch_size batch_timeout controller_script
	   
  Example Usage: startallctl.pl 1000 100 cbench
* stopallctl.pl - stop all RoSCo Controllers
