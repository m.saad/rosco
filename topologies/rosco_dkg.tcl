set ns [new Simulator]
source tb_compat.tcl

# Change this to a number of nodes you want
set NODES 5

set lanstr ""
for {set i 0} {$i < $NODES} {incr i} {
	set dkg($i) [$ns node]
	tb-set-hardware $dkg($i) pc3000
	tb-set-node-os $dkg($i) Ubuntu1804-STD
	append lanstr "$dkg($i) "
}

# Change the BW and delay if you want
set lan0 [$ns make-lan "$lanstr" 1000Mb 0ms]

$ns rtproto Static
$ns run
