# Rosco - multi controller topology
#   using a smartNIC on the switch node

set ns [new Simulator]
source tb_compat.tcl

set CONTROLLERS 4

# Set up the switch - request a smartNIC node
set snsw [$ns node]
tb-fix-node $snsw hpc058
tb-set-node-os $snsw Ubuntu-Agilio-2

# Set up the controllers
set controlPlaneStr "$snsw "
set consensusPlaneStr ""
for {set i 0} {$i < $CONTROLLERS} {incr i} {
    set snctl($i) [$ns node]
    tb-set-node-os $snctl($i) Ubuntu1604-STD
    append controlPlaneStr "$snctl($i) "
	append consensusPlaneStr "$snctl($i) "
}
set controlPlane [$ns make-lan "$controlPlaneStr" 1000Mbps 0ms]
set consensusPlane [$ns make-lan "$consensusPlaneStr" 1000Mbps 0ms]

# Use static routing when mininet is used to emulate hosts
$ns rtProto Static

$ns run
