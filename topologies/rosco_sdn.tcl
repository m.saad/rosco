# Rosco - multi controller topology

set ns [new Simulator]
source tb_compat.tcl

set SWITCHNODES 1
set CONTROLLERS 4

# Set up the switch nodes
set controlPlaneStr ""
for {set i 0} {$i < $SWITCHNODES} {incr i} {
	set snsw($i) [$ns node]
	tb-set-hardware $snsw($i) dl380g3
    tb-set-node-os $snsw($i) Ubuntu1604-STD
    append controlPlaneStr "$snsw($i) "
}

# Set up the controllers
set consensusPlaneStr ""
for {set i 0} {$i < $CONTROLLERS} {incr i} {
    set snctl($i) [$ns node]
    tb-set-node-os $snctl($i) Ubuntu1604-STD
    append controlPlaneStr "$snctl($i) "
	append consensusPlaneStr "$snctl($i) "
}
set controlPlane [$ns make-lan "$controlPlaneStr" 1000Mbps 0ms]
set consensusPlane [$ns make-lan "$consensusPlaneStr" 1000Mbps 0ms]

# Use static routing when mininet is used to emulate hosts
$ns rtProto Static

$ns run
