/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.util.Iterator;
import java.util.LinkedList;


class RoscoRuleDependence
{
	private int xDpId;
	private long xXid;

	RoscoRuleDependence(int dp, long xid) 
	{
		xDpId = dp;
		xXid = xid;
	}

	public final int getDpId()
	{
		return xDpId;
	}

	public final long getXID()
	{
		return xXid;
	}
}

/**
 * Rosco update rule structure
 *
 */

class RoscoRule
{
	private int xDpId;
	private OpenFlowMessage xMsg;
	private RoscoRuleDependence xFlowDep;
	private RoscoRuleDependence xSwitchDep;

	RoscoRule(int dp, OpenFlowMessage msg, RoscoRuleDependence fld, RoscoRuleDependence swd)
	{
		xDpId = dp;
		xMsg = msg;
		xFlowDep = fld;
		xSwitchDep = swd;
	}

	public final int getDpId()
	{
		return xDpId;
	}

	public OpenFlowMessage getMessage()
	{
		return xMsg;
	}

	public final RoscoRuleDependence getFlowDep()
	{
		return xFlowDep;
	}

	public final RoscoRuleDependence getSwitchDep()
	{
		return xSwitchDep;
	}
}

public class RoscoPolicy
{
	private LinkedList<RoscoRule> xRules;

	public RoscoPolicy()
	{
		xRules = new LinkedList<RoscoRule>();
	}

	public boolean isEmpty()
	{
		return xRules.isEmpty();
	}

	public void pushRule(RoscoRule rule)
	{
		synchronized(xRules) {
			xRules.add(rule);
		}
	}

	public RoscoRule pullRule()
	{
		synchronized(xRules) {
			if(xRules.isEmpty()) {
				return null;
			}
			try {
				return xRules.remove();
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	public RoscoRule getNextRule()
	{
		synchronized(xRules) {
			if(xRules.isEmpty()) {
				return null;
			}
			try {
				return xRules.getFirst();
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
