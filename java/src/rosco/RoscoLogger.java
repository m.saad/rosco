/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.io.PrintStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RoscoLogger
{
	public static final int LOGERROR = 1;
	public static final int LOGWARN = 2;
	public static final int LOGINFO = 3;
	public static final int LOGDEBUG = 4;
	
	private static int xId;
	private static int xLogLevel = RoscoLogger.LOGDEBUG;
	private static SimpleDateFormat xDateFormat = new SimpleDateFormat();

	public static void init(RoscoConfigProperties props)
	{
		xId = props.getId(); 
		try {
			xLogLevel = props.getLogLevel();
		} catch (Exception e) {
			xLogLevel = LOGDEBUG;
		}
		String logDir = props.getLogDir();
		String logFile = props.getLogFile();
		if(logDir != null && logFile != null) {
			try {
				String logPath = logDir + "/" + logFile;
				PrintStream outStream = new PrintStream(new FileOutputStream(logPath), true);
				System.setOut(outStream);
				System.setErr(outStream);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void log(int level, String value)
	{
		if(xLogLevel >= level) {
			StringBuffer logMessage = new StringBuffer();
			logMessage.append(xDateFormat.format(new Date())).append(" ").append(xId).append(": ").append(value);
			System.out.println(logMessage.toString());
		}
	}

	public static void logStack(int level, Throwable e)
	{
		if(xLogLevel >= level) {
			e.printStackTrace();
		}
	}

	public static void logStack(int level)
	{
		if(xLogLevel >= level) {
			StackTraceElement[] stack = Thread.currentThread().getStackTrace();
			for(int i = 1; i < stack.length; i++) {
				System.out.println(stack[i].toString());
			}
		}
	}

	public static void logDebug(String value)
	{
		log(LOGDEBUG, value);
	}

	public static void logInfo(String value)
	{
		log(LOGINFO, value);
	}

	public static void logWarn(String value)
	{
		log(LOGWARN, value);
	}

	public static void logError(String value)
	{
		log(LOGERROR, value);
	}
}
