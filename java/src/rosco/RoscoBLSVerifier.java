/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.util.Set;
import java.util.Iterator;

import java.security.MessageDigest;
import it.unisa.dia.gas.jpbc.*;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;

/**
 * Wrap verify a set of BLS signatures
 *
 */

class RoscoBLSSignature
{
	private int xNodeId;
	private byte[] xSignature;

	public RoscoBLSSignature(int n, byte[] s)
	{
		xNodeId = n;
		xSignature = s;
	}

	public int getNodeId()
	{
		return xNodeId;
	}

	public byte[] getSignature()
	{
		return xSignature;
	}
}

public class RoscoBLSVerifier
{
	private Element xPublicKey;
	private Pairing xPairing;
	private Element xU;
	
	// Ensure the libjpbc-pbc is loaded
	static {
		System.load(System.getProperty("libjpbc.path"));
	}

	public RoscoBLSVerifier(RoscoConfigProperties props)
	{
		PairingFactory.getInstance().setUsePBCWhenPossible(true);
		xPairing = PairingFactory.getPairing(props.getPairingParamFile());
		xPublicKey = xPairing.getG1().newElement();
		xPublicKey.setFromString(props.getPublicKey(), 10);
		xU = xPairing.getG1().newElement();
		xU.setFromString(props.getU(), 10);
	}

	private Element apply(Set<RoscoBLSSignature> sigs, int alpha)
	{
		Element falpha = xPairing.getG1().newOneElement();
		for(RoscoBLSSignature sigi : sigs) {
			int i = sigi.getNodeId();
			Element share = xPairing.getG1().newElementFromBytes(sigi.getSignature());
			Element t = xPairing.getZr().newElement(alpha);
			Element coeff = xPairing.getZr().newOneElement();
			for(RoscoBLSSignature sigj : sigs) {
				int j = sigj.getNodeId();
				if(j == i) continue;
				t.set(j - alpha);
				coeff.mul(t);
				t.set(j - i);
				t.invert();
				coeff.mul(t);
			}
			share.powZn(coeff);
			falpha.mul(share);
		}
		return falpha;
	}

	private Element computeMessageSignature(byte[] message)
	{
		try{
			MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
			byte[] msgHash = msdDigest.digest(message);
			Element msgHashG1 = xPairing.getG1().newElementFromHash(msgHash, 0, msgHash.length);
			return xPairing.pairing(xPublicKey, msgHashG1);
		} catch(Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public boolean verify(byte[] message, Set<RoscoBLSSignature> sigShares)
	{
		Element msgSignature = computeMessageSignature(message);
		Element tempSig = apply(sigShares, 0);
		Element vt = xPairing.pairing(xU, tempSig);
		return vt.isEqual(msgSignature);
	}
}
