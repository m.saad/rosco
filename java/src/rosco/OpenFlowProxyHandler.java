/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * OpenFlow Message Proxy Handler
 *
 */

public abstract class OpenFlowProxyHandler
{
	protected AtomicInteger xXid;

	protected static byte OFP_VERSION = 1;

	public OpenFlowProxyHandler()
	{
		xXid = new AtomicInteger(0);
	}

	protected int getNextXID()
	{
		return xXid.getAndIncrement() & 0x7FFFFFFF;
	}

	public abstract void handleNewConnection(ProxyConnection conn);
	public abstract void handleClosedConnection(ProxyConnection conn);
	public abstract void handleOpenFlowMessageFromSwitch(ProxyConnection conn, OpenFlowMessage ofMsg);
	public abstract void handleOpenFlowMessageFromController(ProxyConnection conn, OpenFlowMessage ofMsg);
}
