/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.security.MessageDigest;
import it.unisa.dia.gas.jpbc.*;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;

/**
 * Sign a message using BLS signature share
 *
 */

class RoscoBLSSigner
{
	private Element xShare;
	private Pairing xPairing;
	
	// Ensure the libjpbc-pbc is loaded
	static {
		System.load(System.getProperty("libjpbc.path"));
	}

	public RoscoBLSSigner(RoscoConfigProperties props)
	{
		PairingFactory.getInstance().setUsePBCWhenPossible(true);
		xPairing = PairingFactory.getPairing(props.getPairingParamFile());
		xShare = null;
		setShare(props.getShare());
	}

	public Element sign(byte[] message)
	{
		try {
			System.out.println("Signing my share: " + xShare);
			MessageDigest msdDigest = MessageDigest.getInstance("SHA-1");
			byte[] msgHash = msdDigest.digest(message);
			Element msgHashG1 = xPairing.getG1().newElementFromHash(msgHash, 0, msgHash.length);
			return msgHashG1.powZn(xShare);
		} catch(Throwable e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setShare(String share)
	{
		if(share == null || share.length() == 0) {
			return;
		}
		if(xShare == null) {
			xShare = xPairing.getZr().newElement();
		}
		xShare.setFromString(share, 10);
	}
}
