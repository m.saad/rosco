/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
:limitations under the License.
*/
package rosco;

import java.util.Set;
import java.util.HashSet;

/**
 * Rosco BLS signature test
 *
 */

public class RoscoBLSTest
{
	public static void main(String[] args)
	{
		if(args.length != 2) {
			System.out.println("Usage: java RoscoBLSTest message count");
			System.exit(0);
		}

		String message = args[0];
		int nodeCount = Integer.parseInt(args[1]);

		try {
			Set<RoscoBLSSignature> signatureSet = new HashSet<RoscoBLSSignature>();
			RoscoBLSSigner[] signers = new RoscoBLSSigner[nodeCount];
			for(int i = 0; i < nodeCount; i++) {
				RoscoBLSSigner signer = new RoscoBLSSigner(new RoscoConfigProperties(i));
				signatureSet.add(new RoscoBLSSignature(i+1, signer.sign(message.getBytes()).toBytes()));
			}
			RoscoBLSVerifier verifier = new RoscoBLSVerifier(new RoscoConfigProperties());
			long stime = System.currentTimeMillis();
			System.out.println(verifier.verify(message.getBytes(), signatureSet));
			System.out.println(System.currentTimeMillis() - stime);

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
