/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
:limitations under the License.
*/
package rosco;

/**
 * Rosco controller layer
 *
 */

public class RoscoController
{
	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(char[] bytes) 
	{
		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static void main(String[] args)
	{
		if(args.length != 1) {
			System.out.println("Usage: java RoscoController <id>");
			System.exit(0);
		}

		int id = Integer.parseInt(args[0]);

		try {
			RoscoConfigProperties roscoConfig = new RoscoConfigProperties(id);
			RoscoLogger.init(roscoConfig);
			RoscoControllerLayer roscoLayer = new RoscoControllerLayer(id, roscoConfig);
			roscoLayer.start();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
