/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.io.FileOutputStream;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

import java.net.Socket;

import java.security.MessageDigest;
import it.unisa.dia.gas.jpbc.*;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;

/**
 * Wrap an OpenFlow Message with a BLS signature based on the signature share
 *
 */

public class RoscoBLSConnection extends ProxyConnection
{
	private int xNodeId;
	private RoscoBLSSigner xSigner;

	protected static final Set<Byte> ROSCO_REPLY_TYPES = 
		new HashSet<Byte>(
			// TODO: ADD ALL APPROPRIATE MESSAGE TYPES
			Arrays.asList(
				OpenFlowMessage.OF_PACKETOUT,
				OpenFlowMessage.OF_FLOWMOD
			)
		);

	public RoscoBLSConnection(Socket switch_socket, Socket controller_socket, OpenFlowProxyHandler ofHandler, int node_id, RoscoBLSSigner signer)
	{
		super(switch_socket, controller_socket, ofHandler);
		xNodeId = node_id;
		xSigner = signer;
	}

	public void sendSwitch(OpenFlowMessage ofMsg)
	{
		if(ROSCO_REPLY_TYPES.contains(ofMsg.getType())) {
			super.sendSwitch(new OpenFlowBLSMessage(ofMsg, xNodeId, xSigner.sign(ofMsg.getMessageBytes())));
		} else {
			super.sendSwitch(ofMsg);
		}	
	}
}

class OpenFlowBLSMessage extends OpenFlowMessage
{
	private byte xNodeId;
	private byte xSignatureSize;
	private Element xSignature;
	private OpenFlowMessage xSource;

	private static final byte BLS_TYPE_MASK = (byte)0x80;

	public OpenFlowBLSMessage(OpenFlowMessage source, int node_id, Element signature)
	{
		super(source.getVersion(), (byte)(BLS_TYPE_MASK | source.getType()), (short)(OpenFlowMessage.OF_HEADER_SIZE + 2 + signature.getLengthInBytes() + source.getSize()), source.getXID());
		xMessageBody.put((byte)node_id);
		xMessageBody.put((byte)signature.getLengthInBytes());
		xMessageBody.put(signature.toBytes());
		xMessageBody.put(source.getMessageBytes());
	}
}
