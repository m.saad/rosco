
/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.nio.ByteBuffer;
import java.nio.BufferOverflowException;

/**
 * OpenFlow Message Container
 *
 */

public class OpenFlowMessage
{
	protected byte xVersion;
	protected byte xType;
	protected short xSize;
	protected int xXid;
	protected ByteBuffer xMessageBody;

	public static final byte OF_VERSION_10         = 1;
	public static final byte OF_VERSION_11         = 2;
	public static final byte OF_VERSION_12         = 3;
	public static final byte OF_VERSION_13         = 4;
	public static final byte OF_VERSION_14         = 5;

	public static final byte OF_HELLO              = 0;
	public static final byte OF_ERROR              = 1;
	public static final byte OF_ECHOREQUEST        = 2;
	public static final byte OF_ECHORESPONSE       = 3;
	public static final byte OF_FEATURESREQUEST    = 5;
	public static final byte OF_FEATURESRESPONSE   = 6;
	public static final byte OF_PACKETIN           = 10;
	public static final byte OF_PACKETOUT          = 13;
	public static final byte OF_FLOWMOD            = 14;
	public static final byte OF_10_STATSRESPONSE   = 17;
	public static final byte OF_10_BARRIERREQUEST  = 18;
	public static final byte OF_10_BARRIERRESPONSE = 19;
	public static final byte OF_11_STATSRESPONSE   = 19;
	public static final byte OF_11_BARRIERREQUEST  = 20;
	public static final byte OF_11_BARRIERRESPONSE = 21;

	public static final short OF_HEADER_SIZE = 8;

	protected OpenFlowMessage(byte version, byte type, short size, int xid)
	{
		xVersion = version;
		xType = type;
		xSize = size;
		xXid = xid;
		if(xSize > OF_HEADER_SIZE) {
			xMessageBody = ByteBuffer.allocate(xSize - OF_HEADER_SIZE);
		}
	}

	protected OpenFlowMessage(ByteBuffer messageBytes)
	{
		xVersion = messageBytes.get();
		xType = messageBytes.get();
		xSize = messageBytes.getShort();
		xXid = messageBytes.getInt();
		if(xSize > OF_HEADER_SIZE) {
			xMessageBody = ByteBuffer.allocate(xSize - OF_HEADER_SIZE);
			try {
				xMessageBody.put(messageBytes);
			} catch (BufferOverflowException e) {
				// Intentionally Left Blank
			}
			xMessageBody.rewind();
		}
	}

	public final byte getVersion() { return xVersion; }
	public final byte getType() { return xType; }
	public final short getSize() { return xSize; }
	public final int getXID() { return xXid; }
	public final long getXIDUL() { return Integer.toUnsignedLong(xXid); }
	public void setXID(int xid) { xXid = xid; }

	public byte[] getMessageBytes()
	{
		ByteBuffer dataBuffer = ByteBuffer.allocate(xSize >= OF_HEADER_SIZE ? xSize : OF_HEADER_SIZE);
		dataBuffer.put(xVersion);
		dataBuffer.put(xType);
		dataBuffer.putShort(xSize);
		dataBuffer.putInt(xXid);
		if(xSize > OF_HEADER_SIZE) {
			dataBuffer.put(xMessageBody.array(), 0, xSize - OF_HEADER_SIZE);
		}
		return dataBuffer.array();
	}

	public String toString() 
	{
		return 
			" Version: " + xVersion +
			" Type: " + xType +
			" Size: " + xSize +
			" XID: " + Integer.toHexString(xXid);
	}
}

/**
 * OpenFlowMessage used for testing
 *
 */
class RandomOpenFlowMessage extends OpenFlowMessage
{
	RandomOpenFlowMessage(String body_contents)
	{
		super((byte)0, (byte)4, (short)(OpenFlowMessage.OF_HEADER_SIZE + body_contents.length()), 0);
		xMessageBody.put(body_contents.getBytes());
	}
}

