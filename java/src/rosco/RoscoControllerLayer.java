/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.util.Set;
import java.util.HashSet;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.ListIterator;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import java.net.ServerSocket;
import java.net.Socket;


/**
 * OpenFlow Message Handler Rosco Controller Specific
 *
 */

public class RoscoControllerLayer
{
	private int xNodeId;
	private RoscoEventQueue xEventQueue;
	private EventQueueThread xEventThread;
	private NewSwitchConnectionThread xSwitchConnectionThread;
	private List<ProxyConnection> xConnections;
	private List<RoscoAckSeq> xReceivedAcks;
	private List<SwitchUpdateThread> xUpdateThreads;
	private static final int xMinRoscoXID = 0x80000000;
	private RoscoBLSSigner xBLSSigner;

	private static final Set<Byte> ROSCO_REPLY_TYPES = 
		new HashSet<Byte>(
			// TODO: ADD ALL APPROPRIATE MESSAGE TYPES
			Arrays.asList(
				OpenFlowMessage.OF_PACKETOUT,
				OpenFlowMessage.OF_FLOWMOD
			)
		);

	public RoscoControllerLayer(int id, RoscoConfigProperties props)
	{
		xNodeId = id;
		xEventQueue = new RoscoEventQueue(id, props, this);
		xEventThread = new EventQueueThread();
		xSwitchConnectionThread = new NewSwitchConnectionThread(props.getListenPort(), props.getControllerPort());
		xConnections = new ArrayList<ProxyConnection>();
		xReceivedAcks = new ArrayList<RoscoAckSeq>();
		xUpdateThreads = new ArrayList<SwitchUpdateThread>();
		xBLSSigner = null;
		if (props.useDKG()) {
			System.out.println("Node: " + id + " - Using DKG and BLS");
			xBLSSigner = new RoscoBLSSigner(props);
		}
	}

	public void start()
	{
		Thread runEventThread = new Thread(xEventThread, "ROSCO Event");
		Thread runNewConnectionThread = new Thread(xSwitchConnectionThread, "ROSCO NEW CON");
		xEventQueue.start();
		runEventThread.start();
		runNewConnectionThread.start();
		try {
			runEventThread.join();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		try {
			runNewConnectionThread.join();
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
	}

	private boolean isRoscoReply(OpenFlowMessage ofMsg)
	{
		return ROSCO_REPLY_TYPES.contains(ofMsg.getType());
	}

	private boolean isRoscoEvent(OpenFlowMessage ofMsg)
	{
		return ofMsg.getType() == OpenFlowMessage.OF_PACKETIN;
	}

	private boolean isRoscoAck(OpenFlowMessage ofMsg)
	{
		if(ofMsg.getVersion() == 1 && ofMsg.getType() == OpenFlowMessage.OF_10_BARRIERRESPONSE) {
			return true;
		}
		if(ofMsg.getVersion() > 1 && ofMsg.getType() == OpenFlowMessage.OF_11_BARRIERRESPONSE) {
			return true;
		}
		return false;
	}

	public void shareChanged(String newShare)
	{

	}

	class RoscoAckSeq
	{
		private long xSeqValue;
		
		public RoscoAckSeq()
		{
			xSeqValue = Integer.toUnsignedLong(xMinRoscoXID) - 1;
		}

		public synchronized void checkValueAndWait(long value)
		{
			while(xSeqValue < value) {
				// System.out.println("F CHECK DEP: " + xSeqValue + " " + value);
				try {
					wait();
					//System.out.println("WAKE: " + xSeqValue + " " + value);
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
			// System.out.println("T CHECK DEP: " + xSeqValue + " " + value);
		}

		public synchronized void setValueAndNotify(long value)
		{
			if(value == xSeqValue + 1) {
				xSeqValue = value;
				notifyAll();
			}
		}
	}

	class RoscoProxyHandler extends OpenFlowProxyHandler
	{
		private int xIndex;

		RoscoProxyHandler(int idx)
		{
			xIndex = idx;
		}
	
		private int getNextRoscoXID()
		{
			return getNextXID() | xMinRoscoXID; 
		}

		public void handleNewConnection(ProxyConnection conn) {}
		public void handleClosedConnection(ProxyConnection conn) {}
	
		public synchronized void handleOpenFlowMessageFromController(ProxyConnection conn, OpenFlowMessage ofMsg)
		{
			if(!isRoscoReply(ofMsg)) {
				conn.sendSwitch(ofMsg);
				return;
			}
			int msg_xid = getNextRoscoXID();
			ofMsg.setXID(msg_xid);
			handlePolicyReceived(xIndex, conn, ofMsg);
		}

		public synchronized void handleOpenFlowMessageFromSwitch(ProxyConnection conn, OpenFlowMessage ofMsg)
		{
			if(isRoscoEvent(ofMsg)) {
				xEventQueue.enqueueEvent(new RoscoEvent(ofMsg.getXID(), xIndex, ofMsg));
			} else if(isRoscoAck(ofMsg)) {
				handleAckReceived(xIndex, conn, ofMsg);
			} else {
				conn.sendController(ofMsg);
			}
		}
	}

	class EventQueueThread implements Runnable
	{
		private boolean xRunning;

		EventQueueThread()
		{
			xRunning = false;
		}

		public void stop()
		{
			xRunning = false;
		}

		public void run()
		{
			xRunning = true;
			while(xRunning) {
				List<RoscoEvent> eventBatch = xEventQueue.dequeueEventBatch();
				Iterator<RoscoEvent> iterator = eventBatch.iterator();
		  		while(iterator.hasNext()) {
					RoscoEvent event = iterator.next();
					xConnections.get(event.getDpId()).sendController(event.getMessage());
				}
				//System.out.println("EV: " + event.getMessage().toString());
			}
		}
	}

	class NewSwitchConnectionThread implements Runnable
	{
		private int xPort;
		private int xControllerPort;
		private boolean xRunning;

		NewSwitchConnectionThread(int port, int ctl_port)
		{
			xPort = port;
			xControllerPort = ctl_port;
			xRunning = false;
		}

		public void stop()
		{
			xRunning = false;
		}

		public void run()
		{
			try {
				xRunning = true;
				int current_dp = 0;
				ServerSocket serverSocket = new ServerSocket(xPort);
				while(xRunning) {
					Socket switchSocket = serverSocket.accept();
					System.out.println("New Connection From: " + switchSocket.getInetAddress().toString() + ":" + switchSocket.getPort());
	
					Socket controllerSocket = new Socket("localhost", xControllerPort);
					OpenFlowProxyHandler ofHandler = new RoscoProxyHandler(current_dp);
					ProxyConnection conn = null;
					if(xBLSSigner != null) {
						conn = new RoscoBLSConnection(switchSocket, controllerSocket, ofHandler, xNodeId+1, xBLSSigner);
					} else {
						conn = new ProxyConnection(switchSocket, controllerSocket, ofHandler);
					}
					SwitchUpdateThread updateThread = new SwitchUpdateThread();
					xConnections.add(current_dp, conn);
					xReceivedAcks.add(current_dp, new RoscoAckSeq());
					xUpdateThreads.add(current_dp, updateThread);
					conn.start();
					updateThread.start();
					current_dp++;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	class PolicyUpdateThread implements Runnable
	{
		RoscoPolicy xPolicy;
		boolean xRunning;

		public PolicyUpdateThread(RoscoPolicy p)
		{
			xPolicy = p;
			xRunning = false;
		}

		public void start() 
		{
			xRunning = true;
			new Thread(this).start();
		}

		public void stop()
		{
			xRunning = false;
		}

		private void waitDepFullfilled(RoscoRuleDependence dep)
		{
			if (dep == null) {
				return;
			}
			xReceivedAcks.get(dep.getDpId()).checkValueAndWait(dep.getXID());
		}

		private void waitToSendRule(RoscoRule rule)
		{
			waitDepFullfilled(rule.getFlowDep());
			waitDepFullfilled(rule.getSwitchDep());
		}

		public void run()
		{
			try {
				while(xRunning) {
					RoscoRule nextRule = xPolicy.getNextRule();
					waitToSendRule(nextRule);
					xConnections.get(nextRule.getDpId()).sendSwitch(nextRule.getMessage());
					xPolicy.pullRule();
					if (xPolicy.isEmpty()) {
						return;
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	class SwitchUpdateThread implements Runnable
	{
		private BlockingQueue<RoscoRule> xRuleQueue;
		boolean xRunning;

		public SwitchUpdateThread()
		{
			xRunning = false;
			xRuleQueue = new LinkedBlockingQueue<RoscoRule>();
		}
		
		public void start() 
		{
			xRunning = true;
			new Thread(this).start();
		}

		public void stop()
		{
			xRunning = false;
		}

		private void waitDepFullfilled(RoscoRuleDependence dep)
		{
			if (dep == null) {
				return;
			}
			xReceivedAcks.get(dep.getDpId()).checkValueAndWait(dep.getXID());
		}

		private void waitToSendRule(RoscoRule rule)
		{
			waitDepFullfilled(rule.getFlowDep());
			waitDepFullfilled(rule.getSwitchDep());
		}

		public void addRule(RoscoRule r)
		{
			try {
				xRuleQueue.put(r);
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void run()
		{
			while(xRunning) {
				try {
					RoscoRule nextRule = xRuleQueue.take();
					waitToSendRule(nextRule);
					xConnections.get(nextRule.getDpId()).sendSwitch(nextRule.getMessage());
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void handleAckReceived(int index, ProxyConnection conn, OpenFlowMessage ofMsg)
	{
		//System.out.println("ACK RECV: " + Integer.toUnsignedLong(ofMsg.getXID()));
		xReceivedAcks.get(index).setValueAndNotify(Integer.toUnsignedLong(ofMsg.getXID()));
	}
	
	public void handlePolicyReceived(int index, ProxyConnection conn, OpenFlowMessage ofMsg)
	{
		synchronized(xReceivedAcks) {
			//System.out.println("Policy Recieved: " + index + " " + ofMsg);
			//RoscoPolicy newPolicy = new RoscoPolicy();
			//RoscoRule newRule = new RoscoRule(index, ofMsg, null, new RoscoRuleDependence(index, Integer.toUnsignedLong(ofMsg.getXID()) - 1));
			//newPolicy.pushRule(newRule);
			//new Thread(new PolicyUpdateThread(newPolicy), "Policy: " + Integer.toUnsignedLong(ofMsg.getXID())).start();
			RoscoRule newRule = new RoscoRule(index, ofMsg, null, new RoscoRuleDependence(index, Integer.toUnsignedLong(ofMsg.getXID()) - 1));
			//RoscoRule newRule = new RoscoRule(index, ofMsg, null, null);
			xUpdateThreads.get(index).addRule(newRule);
		}
	}
}
