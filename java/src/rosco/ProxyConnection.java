/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
:limitations under the License.
*/
package rosco;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Openflow Proxy Connection
 *
 */

class ProxyConnection
{
	private boolean xRunning;
	
	private Socket xSwitchSocket;
	private SwitchConnectionThread xSwitchThread;
	private OpenFlowMessageReader xFromSwitchStream;
	private OpenFlowMessageWriter xToSwitchStream;

	private Socket xControllerSocket;
	private ControllerConnectionThread xControllerThread;
	private OpenFlowMessageReader xFromControllerStream;
	private OpenFlowMessageWriter xToControllerStream;

	private OpenFlowProxyHandler xOpenFlowHandler;

	ProxyConnection(Socket switch_socket, Socket controller_socket, OpenFlowProxyHandler ofHandler)
	{
		xOpenFlowHandler = ofHandler;
		xRunning = false;

		if(switch_socket != null) {
			xSwitchSocket = switch_socket;
			try {
				xFromSwitchStream = new OpenFlowMessageReader(switch_socket.getInputStream());
				xToSwitchStream = new OpenFlowMessageWriter(switch_socket.getOutputStream());
				xSwitchThread = new SwitchConnectionThread(this);
			} catch(IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		
		if(controller_socket != null) {
			xControllerSocket = controller_socket;
			try {
				xFromControllerStream = new OpenFlowMessageReader(controller_socket.getInputStream());
				xToControllerStream = new OpenFlowMessageWriter(controller_socket.getOutputStream());
				xControllerThread = new ControllerConnectionThread(this);
			} catch(IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	public String toString()
	{
		return "ProxyConnection: SW: " + xSwitchSocket + " CTL: " + xControllerSocket;
	}

	private void socketDisconnect(Socket sock)
	{
		try {
			System.out.println("DISCONNECTING FROM: " + sock);
			sock.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public void disconnect()
	{
		socketDisconnect(xSwitchSocket);
		socketDisconnect(xControllerSocket);
		xRunning = false;
	}

	public void sendSwitch(OpenFlowMessage ofMsg)
	{
		if(xToSwitchStream.write(ofMsg) == false) {
			disconnect();
		}
	}

	public void sendController(OpenFlowMessage ofMsg)
	{
		if(xToControllerStream.write(ofMsg) == false) {
			disconnect();
		}
	}

	public void start()
	{
		xRunning = true;
		if(xSwitchThread != null) {
			new Thread(xSwitchThread, "PROXY SWITCH: " + xSwitchSocket).start();
		}
		if(xControllerThread != null) {
			new Thread(xControllerThread, "PROXY CONTROLLER " + xControllerSocket).start();
		}
		xOpenFlowHandler.handleNewConnection(this);
	}

	class SwitchConnectionThread implements Runnable
	{
		private ProxyConnection xProxy;

		SwitchConnectionThread(ProxyConnection conn)
		{
			xProxy = conn;
		}
		
		public void run()
		{
			while(xRunning) {
				OpenFlowMessage ofMsg = xFromSwitchStream.read();
				if(ofMsg == null) {
					xProxy.disconnect();
					return;
				}
				xOpenFlowHandler.handleOpenFlowMessageFromSwitch(xProxy, ofMsg);
			}
		}
	}
	
	class ControllerConnectionThread implements Runnable
	{
		private ProxyConnection xProxy;

		ControllerConnectionThread(ProxyConnection conn)
		{
			xProxy = conn;
		}
		
		public void run()
		{
			while(xRunning) {
				OpenFlowMessage ofMsg = xFromControllerStream.read();
				if(ofMsg == null) {
					xProxy.disconnect();
					return;
				}
				xOpenFlowHandler.handleOpenFlowMessageFromController(xProxy, ofMsg);
			}
		}
	}
}
