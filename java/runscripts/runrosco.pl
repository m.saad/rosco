#!/usr/bin/perl

use strict;
use File::Basename;
use Cwd 'abs_path';
use Getopt::Std;

my $jpbc_lib = "lib/libjpbc-pbc.so";

our($opt_f);
getopts('f');

my $class = shift;
my $ctl_id = shift;
my $src_dir = dirname(dirname(abs_path($0)));
print_usage() unless(defined($ctl_id) && defined($class));

#my $jdb_opts = "-agentlib:jdwp=transport=dt_socket,address=5555,server=y,suspend=n";
my $jdb_opts = "";

my $java_cmd = sprintf("java $jdb_opts -Dlibjpbc.path=$src_dir/$jpbc_lib -cp $src_dir/bin/*:$src_dir/lib/*:$src_dir/config -Xmx4096M rosco.%s %s %s", $class, $ctl_id, join(' ', @ARGV));
#print "$java_cmd\n";

if($opt_f) {
	if (fork() == 0) {
		chdir $src_dir or die "Unable to chdir to $src_dir: $!\n";
		exec $java_cmd;
		exit(1);
	}
} else {
	chdir $src_dir or die "Unable to chdir to $src_dir: $!\n";
	system($java_cmd);
}

sub print_usage {
	my $file_name = basename($0);
    die "Run RoSCo App\nUsage: $file_name [-f] rosco_class controller_id parms...\n";
}
