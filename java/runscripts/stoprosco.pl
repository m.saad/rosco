#!/usr/bin/perl

use strict;
use File::Basename;

my $ctl_id = shift;
my $class = shift;
print_usage() unless(defined($ctl_id) && defined($class));

for(split(/\n/, `ps aux | grep java | grep rosco`)) {
	if(/^\w+\s+(\d+).*rosco\.$class $ctl_id/) {
		system("kill $1");
	}
}

sub print_usage {
    my $file_name = basename($0);
    die "Stop RoSCo App\nUsage: $file_name controller_id rosco_class\n";
}
