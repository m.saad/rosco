import os
import time
import numpy as np
import re
import glob
from mininet.topo import Topo
from mininet.node import OVSKernelSwitch
from subprocess import Popen, PIPE, STDOUT
#from iperf_util import generate_flows, runFlows, runFlowsRounds
from itg_util import runFlows, runFlowsRounds

core_routers = 4
edge_routers = 2
rack_hosts = 4
link_bandwidth = 10
link_delay = '.001ms'
double_link_delay = '.002ms'
default_flow_count = 100

def _increment_wrap(value, base_value, max_value):
    new_value = value + 1
    if(new_value == max_value):
        return base_value
    return new_value

def _get_next_in_rack_host(host_id, current_target=None):
    group_id = host_id // rack_hosts
    base_id = group_id * rack_hosts
    max_id = base_id + rack_hosts
    new_target = current_target
    if(new_target is None):
        new_target = base_id
    else:
        new_target = _increment_wrap(current_target, base_id, max_id)
    while(new_target == host_id):
        new_target = _increment_wrap(new_target, base_id, max_id)
    return new_target

def _get_next_out_of_rack_host(host_id, current_target=None):
    group_id = host_id // rack_hosts
    base_id = 0
    max_id = edge_routers * rack_hosts
    new_target = current_target
    if(new_target is None):
        new_target = base_id
    else:
        new_target = _increment_wrap(current_target, base_id, max_id)
    while(new_target // rack_hosts == group_id):
        new_target = _increment_wrap(new_target, base_id, max_id)
    return new_target

hadoop_params = {
    'in_rack_ratio': 0.13,
    'inter_arrival': 2,
    'in_rack_params': {
        'packet_size': 250,
        'flow_size': 100000,
        'next_host': _get_next_in_rack_host,
    },
    'out_of_rack_params': {
        'packet_size': 250,
        'flow_size': 500,
        'next_host': _get_next_out_of_rack_host,
    },
};
webserver_params = {
    'in_rack_ratio': 0.12,
    'inter_arrival': 2,
    'in_rack_params': {
        'packet_size': 175,
        'flow_size': 1000,
        'next_host': _get_next_in_rack_host,
    },
    'out_of_rack_params': {
        'packet_size': 175,
        'flow_size': 1000,
        'next_host': _get_next_out_of_rack_host,
    },
};

def _runFlows(net, params, rounds=None, flows_per_round=None, time=None):
    inter_arrival = params['inter_arrival']
    if(time is not None):
        total_flows = _time // inter_arrival
    elif(rounds is not None and flows_per_round is not None):
        total_flows = flows_per_round * rounds
    else:
        total_flows = default_flow_count;
    if(rounds is None):
        return runFlows(net, params, total_flows)
    else:
        return runFlowsRounds(net, params, rounds, flows_per_round)

def runHadoopFlows(net, rounds=None, flows_per_round=default_flow_count, time=None):
    return _runFlows(net, hadoop_params, rounds=rounds, flows_per_round=flows_per_round, time=time)
def runWebServerFlows(net, rounds=None, flows_per_round=default_flow_count, time=None):
    return _runFlows(net, webserver_params, rounds=rounds, flows_per_round=flows_per_round, time=time)
    
class FacebookTopo(Topo):
    "Creates a Facebook database configuration 4 interconnected cells"
    def build(self, n=2):
        """The baseline facebook topology has 4 core routers and 38 edge
        routers that make up a pod"""
        #edge routers are also known as Top of the Rack (TOR) Routers
        switch_type = OVSKernelSwitch

        switches = {}
        hosts = {}
        print '*** Adding the core routers                             ***'
        for x in range(0, 1):
            switches[x] = self.addSwitch('cr' + str(x), switch=switch_type)
        print '*** Adding the edge routers                             ***'
        for x in range(0, edge_routers):
            switches[x+core_routers] = self.addSwitch('er' + str(x), switch=switch_type)
            for h in range(0, rack_hosts):
                host_id = x*rack_hosts + h
                hosts[host_id] = self.addHost('h'+str(host_id))
                self.addLink(hosts[host_id], switches[x+core_routers], bw=link_bandwidth, delay=link_delay)
        """
        *** Adding links to construct topology                ***
        *********************************************************
        *     cr1        cr2           cr3           cr4        *
        *                                                       *
        *                                                       *
        *                                                       *
        *                                                       *
        * er1  er2  er3  er4  er5  er6  er7  er8  er9 ...  er48 *
        *                                                       *
        *********************************************************
        """
        for x in range(0, 1):
            #the core router that we want to connect
            for i in range(0, edge_routers):
                #the TOR router that we want to connect the core to
                self.addLink(switches[x], switches[i+core_routers], bw=link_bandwidth*4, delay=double_link_delay)
