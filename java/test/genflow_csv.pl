#!/usr/bin/perl

use strict;

my $flow_times;
my $flow_durations;
my $iteration = 0;
while(<flowout_*>) {
	my $line_count = 0;
	for(split(/\n/, `cat $_`)) {
		chomp;
		if($line_count % 2 == 0) {
			$flow_times->{$iteration} = [split(/,/, $_)];
		} else {
			$flow_durations->{$iteration} = [split(/,/, $_)];
		}
		$line_count += 1;
	}
	$iteration += 1;
}

my $max_results = 0;
for(keys %{$flow_times}) {
	my $ci = $_;
	if(scalar @{$flow_times->{$ci}} > $max_results) {
		$max_results = scalar @{$flow_times->{$ci}};
	}
}
for (0 .. $max_results-1) {
	my $index = $_;
	for(keys %{$flow_times}) {
		my $ci = $_;
		if(defined($flow_times->{$ci}->[$index])) {
			print $flow_times->{$ci}->[$index];
		}
		print ",";
		if(defined($flow_durations->{$ci}->[$index])) {
			print $flow_durations->{$ci}->[$index];
		}
		print ",";
	}
	print "\n";
}
