use strict;

use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));

my $rosco_app_path = dirname($script_dir);
my $rosco_test_path = $script_dir;
my $log_dir = "/tmp/rosco/log";

my $ryu_log_level = 30;
my $rosco_switch_port = 6633;
my $rosco_ctl_port = 4000;

my $rosco_std_type = 1;
my $stock_ryu_type = 2;

$| = 1;

# Controller utilities
sub getControllers {
	my $max_count = shift;
	my $controllers;
	my @controller_ips = (
		'10.1.2.3',
		'10.1.2.4',
		'10.1.2.5',
		'10.1.2.6'
	);
	my $count = 0;
	for(@controller_ips) {
		$controllers->{$count++} = $_;
		if(defined($max_count) and $max_count == $count) {
			last;
		}
	}	
	return $controllers;
}

sub startQueueTest {
	my $batch_size = shift;
	my $batch_count = shift;
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
		my $id = $_;
		my $ctl = $controllers->{$id};
		print "Starting RoSCo Queue Test $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_app_path/runscripts/runrosco.pl -f RoscoQueueTest $id $batch_size $batch_count");
	}
}
sub stopQueueTest {
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
        my $id = $_;
        my $ctl = $controllers->{$id};
        print "Stopping RoSCo Queue Test $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_app_path/runscripts/stoprosco.pl $id RoscoQueueTest");
	}
}

sub startRoscoServers {
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
		my $id = $_;
		my $ctl = $controllers->{$id};
		print "Starting RoSCo $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_app_path/runscripts/runrosco.pl -f RoscoProxyMessageServer $id &");
	}
}
sub stopRoscoServers {
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
        my $id = $_;
        my $ctl = $controllers->{$id};
        print "Stopping RoSCo $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_app_path/runscripts/stoprosco.pl $id RoscoProxyMessageServer");
	}
}

sub startRyuServers {
	my $controller_app = shift;
	my $port = shift;
	my $count = shift;
	my $controllers = getControllers($count);
	for (sort keys %{$controllers}) {
		my $id = $_;
		my $ctl = $controllers->{$id};
		print "Starting Ryu $id $ctl $controller_app\n";
		system_or_exit("ssh $ctl $rosco_test_path/startryuctl $id $rosco_test_path/$controller_app.py $log_dir $port $ryu_log_level");
	}
}
sub stopRyuServers {
	my $controllers = getControllers();
	for (sort keys %{$controllers}) {
        my $id = $_;
        my $ctl = $controllers->{$id};
		print "Stopping Ryu $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_test_path/stopryuctl $id");
	}
}

sub startRoscoServersWithMalicious {
	my $controllers = getControllers();
	my $last_ctl = scalar(keys %{$controllers}) - 1;
	for (sort keys %{$controllers}) {
		my $id = $_;
		last if $id eq $last_ctl;
		my $ctl = $controllers->{$id};
		print "Starting RoSCo $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_app_path/runscripts/runrosco.pl -f RoscoProxyMessageServer $id");
	}
	print "Starting RoSCo Malicious\n";
	my $ctl = $controllers->{$last_ctl};
	system_or_exit("ssh $ctl $rosco_app_path/runscripts/runrosco.pl -f RoscoMalicious $last_ctl");
}
sub stopRoscoServersWithMalicious {
	my $controllers = getControllers();
	my $last_ctl = scalar(keys %{$controllers}) - 1;
	for (sort keys %{$controllers}) {
        my $id = $_;
		last if $id eq $last_ctl;
        my $ctl = $controllers->{$id};
        print "Stopping RoSCo $id $ctl\n";
		system_or_exit("ssh $ctl $rosco_app_path/runscripts/stoprosco.pl $id RoscoProxyMessageServer");
	}
	print "Stopping RoSCo Malicious\n";
	my $ctl = $controllers->{$last_ctl};
	system_or_exit("ssh $ctl $rosco_app_path/runscripts/stoprosco.pl $last_ctl RoscoMalicious");
}

sub startControllers {
	my $type = shift;
	my $controller_app = shift;
	my $port = $rosco_ctl_port;
	my $count = undef;
	if($type == $rosco_std_type) {
		startRoscoServers();
		sleep 15;
	} elsif($type == $stock_ryu_type) {
		$count = 1;
		$port = $rosco_switch_port;
	}
	startRyuServers($controller_app, $port, $count);
}
sub stopControllers {
	my $type = shift;
	stopRyuServers();
	if($type == $rosco_std_type) {
		stopRoscoServers();
	}
}
sub restartControllers {
	stopControllers(@_);
	startControllers(@_);
}

sub startControllersWithMalicious {
	my $controller_app = shift;
	startRoscoServersWithMalicious();
	sleep 15;
	startRyuServers($controller_app);
}
sub stopControllersWithMalicous {
	stopRyuServers();
	stopRoscoServersWithMalicious();
}
sub restartControllersWithMalicious {
	stopControllers();
	startControllersWithMalicious(@_);
}

# Mininet utilities
sub reset_mn {
	system_or_exit("sudo mn -c");
	if($? != 0) {
		die "Unable to clean mininet config: $!\n";
	}
}
sub reset_exec_mn {
	my $script = shift;
	my ($mn_out, $mn_in);

	reset_mn;
	print "Starting mininet: $script\n";
	my $python_cmd = "sudo python -u $rosco_test_path/$script @_";
	print "$python_cmd\n";
	my $mn_pid = open3($mn_in, $mn_out, $mn_out, $python_cmd);

	my $inchar;
	my $loc = 0;
	while(1) {
		read $mn_out, $inchar, 1, $loc;
		last if($inchar =~ /mininet> /);
		if($inchar =~ /\n$/) {
			print $inchar;
			$inchar = "";
			$loc = 0;
		} else {
			$loc++;
		}
	}

	return ($mn_pid, $mn_out, $mn_in);
}
sub exit_mn {
	my ($mn_pid, $mn_out, $mn_in, $ovs_out) = @_;
	print $mn_in "exit\n";
	if(defined($ovs_out)) {
		wait_for_end_ovs_log($ovs_out, 1);
		kill 'INT', $mn_pid;
	}
	waitpid $mn_pid, 0;
}

sub system_or_exit {
	my $cmd = shift;
	if(do_system($cmd) != 0) {
		die "$cmd failed: $?\n";
	}
}

sub do_system {
	my $cmd = shift;
	print("$cmd\n");
	my $retval = system($cmd);
	return $retval >> 8;
	#return 0;
}

return 1;
