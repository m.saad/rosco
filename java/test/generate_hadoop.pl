#!/usr/bin/perl

use strict;


# Fan out of 3 TOR + Row + Network
my $depth = shift;
my $fanout = shift;
my $script_base_name = shift;

my $concurrent_flows = 25;
my $in_rack_ratio = 0.25;
my $in_rack_packet_size = 250;
my $in_rack_flow_duration = 5;
my $in_rack_flow_size = 100000;
my $out_of_rack_packet_size = 250;
my $out_of_rack_flow_duration = 500;
my $out_of_rack_flow_size = 500;

my $total_hosts = $fanout**$depth;
my $total_in_rack_flows = int($concurrent_flows * $in_rack_ratio);
my $total_out_of_rack_flows = int($concurrent_flows * (1 - $in_rack_ratio));
my $in_rack_packet_count = int($in_rack_flow_size / $in_rack_packet_size);
my $out_of_rack_packet_count = int($out_of_rack_flow_size / $out_of_rack_packet_size);

for(0 .. $total_hosts-1) {
	my $host_id = $_;
	my $group_id = int($host_id / $fanout);
	my $base_id = $group_id * $fanout;
	my $max_id = $base_id + $fanout;

	printf "%d:\n", $host_id+1;
	my $scr_filename = sprintf "%s_h%d", $script_base_name, $host_id+1;
	open SCRFILE, ">", $scr_filename or die "Unable to open $scr_filename: $!\n";
	my $current_target = undef;
	for(0 .. $total_in_rack_flows-1) {
		$current_target = get_next_in_rack_host($host_id, $current_target);
		printf SCRFILE "-a h%d -t %d -z %d -T UDP -o %d\n", $current_target+1, $in_rack_flow_duration, $in_rack_packet_count, $in_rack_packet_size;
	}
	$current_target = undef;
	for(0 .. $total_out_of_rack_flows-1) {
		$current_target = get_next_out_of_rack_host($host_id, $current_target);
		printf SCRFILE "-a h%d -t %d -z %d -T UDP -o %d\n", $current_target+1, $out_of_rack_flow_duration, $out_of_rack_packet_count, $out_of_rack_packet_size;
	}
	close SCRFILE;
}

sub get_next_in_rack_host {
	my $host_id = shift;
	my $current_target = shift;
	
	my $group_id = int($host_id / $fanout);
	my $base_id = $group_id * $fanout;
	my $max_id = $base_id + $fanout;

	if(!defined($current_target)) {
		$current_target = $base_id;
	} else {
		$current_target = increment_wrap($current_target, $base_id, $max_id);
	}
	while($current_target == $host_id) {
		$current_target = increment_wrap($current_target, $base_id, $max_id);
	}
	return $current_target;
}

sub get_next_out_of_rack_host {
	my $host_id = shift;
	my $current_target = shift;
	
	my $group_id = int($host_id / $fanout);
	my $base_id = 0;
	my $max_id = $total_hosts;
	
	if(!defined($current_target)) {
		$current_target = 0;
	} else {
		$current_target = increment_wrap($current_target, $base_id, $max_id);
	}
	while(int($current_target / $fanout) == $group_id) {
		$current_target = increment_wrap($current_target, $base_id, $max_id);
	}
	return $current_target;
}

sub increment_wrap {
	my $value = shift;
	my $base = shift;
	my $max = shift;
	my $new_value = $value + 1;
	if($new_value == $max) {
		return $base;
	}
	return $new_value;
}
