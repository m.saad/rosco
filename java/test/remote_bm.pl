#!/usr/bin/perl

use strict;
use strict;
use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
require "$script_dir/common.pl";

my $benchmark_cmd = "sudo ovs-appctl connmgr/benchmark";

my $msg_count = shift;
my $switch_count = shift;

my $switches = join(" ", map { "s$_" } (1 .. $switch_count));
for(@ARGV) {
	system_or_exit("ssh $_ $benchmark_cmd $msg_count $switches &");
}
